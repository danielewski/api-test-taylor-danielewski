import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BitBucket_Access {
	public static void main(String[] args) throws IOException {

		// Make call to GET data from the BitBucket API & run data through inputStream
		String command = "curl GET https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits";
		ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
		Process process = processBuilder.start();
		java.io.InputStream inputStream = process.getInputStream();

		BufferedWriter writer = new BufferedWriter(new FileWriter("BitBucket_Commit_Messages.txt"));

		List<String> messages = new ArrayList<>();
		int nextBit = 0;
		String newMessage = "";
		boolean skipOne = false;

		while (nextBit != -1) {
			nextBit = inputStream.read();

			// Check for <p> tag, signifying beginning of message
			if ((char) nextBit == '<') {
				String tag = "" + (char) nextBit;
				for (int i = 0; i < 2; i++) {
					nextBit = inputStream.read();
					tag = tag + (char) nextBit;
				}

				if (tag.equals("<p>")) {

					while (nextBit != -1) {
						nextBit = inputStream.read();

						// Check for closing tag, signifying end of message
						if ((char) nextBit == '<') {
							if (!skipOne) {
								messages.add(newMessage);
							}
							skipOne = !skipOne;
							newMessage = "";
							break;

						} else {
							if ((char) nextBit != '\\') {
								newMessage = newMessage + (char) nextBit;

							}
						}
					}
				}
			}

		}

		// Add messages to the file in chronological order
		for (int i = messages.size() - 1; i >= 0; i--) {
			writer.write(messages.get(i));
			writer.newLine();
		}

		writer.close();
		process.destroy();
		
		System.out.println("Finished.\nMessages Located In: BitBucket_Commit_Messages.txt");
	}
}
